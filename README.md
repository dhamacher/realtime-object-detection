# Purpose
This is based of the [Real-time OCR and Text Detection with Tensorflow, OpenCV and Tesseract](https://www.coursera.org/learn/ocr-text-detection-tensorflow-opencv-tesseract/home/welcome) guided project from Coursera on how to build a real time OCR text detection program. I am training my own model so I had to do some digging and make some code changes because of the Tensorflow 1 and 2 migration. I installed Tensorflow 2 on Python 3.8 with protoc 3.18. I copied the object detection model into my root folder. Since I have build this project on Windows, I also included the binary for **labelImg** which 
is useful for labeling image that you want to represent your ROI (region of interest)

# Run a Pretrained Model
This sections outlines how to run a prebuild model.

Choose a model from the [Tensorflow 2 Model Zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md)
The particular detection algorithm we will use is the `faster_rcnn_resnet50_v1_640x640_coco17_tpu-8`.
To use a different model you will need the URL name of the specific model. This can be done as
follows:

1. Right click on the `Model name` of the model you would like to use;
2. Click on `Copy link address` to copy the download link of the model;
3. Paste the link in a text editor of your choice. You should observe a link similar to ``download.tensorflow.org/models/object_detection/tf2/YYYYYYYY/XXXXXXXXX.tar.gz``;
4. Copy the ``XXXXXXXXX`` part of the link and use it to replace the value of the ``MODEL_NAME`` variable in the code shown in `realtime_object_detection.py` (line 39);
5. Copy the ``YYYYYYYY`` part of the link and use it to replace the value of the ``MODEL_DATE`` variable in the code shown in `realtime_object_detection.py` (line 38).

For example, the download link for the model used below is: ``download.tensorflow.org/models/object_detection/tf2/20200711/ssd_resnet101_v1_fpn_640x640_coco17_tpu-8.tar.gz``

Once the steps are complete you can execute the script using

        python realtime_object_detection.py



# My Development Environment 
* Windows 10 Pro 64-bit
* Tensorflow 2.0
* Protobuf 3.17
* Python 3.8

# Training Your Own Model
This section is for training your own model. You do not need to go through this guide unless you want 
to do the same thing.

## Environment Setup
1. Install **Python 3.8** and create a folder for your project. Then create a virtual environment 
   with `python -m venv venv`
2. Create a new folder at a different location for the Tensorflow 2 Object Detection API, then clone the Tensorflow 
   Models to your local computer

        git clone https://github.com/tensorflow/models

3. Copy the object detection model under **\\models\\research\\** to your project root directory
4. For **Windows** install the MS build tools which is required for a variety of packages.
6. Install [protobuf](https://github.com/protocolbuffers/protobuf/releases/)
7. Run this command in your project root folder:

        for /f %i in ('dir /b object_detection\protos\*.proto') do protoc object_detection\protos\%i --python_out=.

8. Install requirements with `pip install -r requirements.txt`

## Image Data Recommendations
100-1000 images maybe even more.
* image size below 200kb
* image max size 720x1280
* 20% of images to keep for training

## Image labeling and Data Preprocessing
1. Use [labelImg](https://github.com/tzutalin/labelImg#labelimg) to label your image data.
2. Create a label map such like **\inference_graph\training\label_map.pbtxt** which is of the form

                item {
                        id: 1
                        name: '<label_tag_used>'
                }

3. Convert the xml files from the train directory to tfrecord using:

        python generate_tfrecord.py -x C:\Users\dhama\Projects\realtime-ocr\images\train -l C:\Users\dhama\Projects\realtime-ocr\inference_graph\training\label_map.pbtxt -o C:\Users\dhama\Projects\realtime-ocr\images\tfrecord\train.record

4. Convert the xml files from the test directory to tfrecord using:

        python generate_tfrecord.py -x C:\Users\dhama\Projects\realtime-ocr\images\test -l C:\Users\dhama\Projects\realtime-ocr\inference_graph\training\label_map.pbtxt -o C:\Users\dhama\Projects\realtime-ocr\images\tfrecord\test.record


## Train your model
TODO

# Resource
1. I partially used [this](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/install.html#tf-install) tutorial to get my environment up and running.

2. [Tutorial for using the object detection API](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/install.html#tf-install)

3. Read about the [TensorFlow Object Detection API](https://github.com/tensorflow/models/tree/master/research/object_detection)

4. Read about [Tensorflow in docker](https://www.tensorflow.org/install/docker)

5. https://medium.com/@armandj.olivares/text-detection-on-natural-scenes-with-tensorflow-object-detection-api-bfa936d3d103


