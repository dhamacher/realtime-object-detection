#!/usr/bin/env python
# coding: utf-8
"""
Real Time Object Detection using Tensorflow 2 and Open CV.

"""
import cv2
import os
from pathlib import Path
import tarfile
import urllib.request
import tensorflow as tf
from object_detection.utils import label_map_util
from object_detection.utils import config_util
from object_detection.utils import visualization_utils as viz_utils
from object_detection.builders import model_builder
import numpy as np
"""
Download a model

The code snippet shown below is used to download the object detection model checkpoint file,
as well as the labels file (.pbtxt) which contains a list of strings used to add the correct
label to each detection (e.g. person).

The particular detection algorithm we will use is the `SSD ResNet101 V1 FPN 640x640`. More
models can be found in the `TensorFlow 2 Detection Model Zoo https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md.
To use a different model you will need the URL name of the specific model. This can be done as
follows:

1. Right click on the `Model name` of the model you would like to use;
2. Click on `Copy link address` to copy the download link of the model;
3. Paste the link in a text editor of your choice. You should observe a link similar to ``download.tensorflow.org/models/object_detection/tf2/YYYYYYYY/XXXXXXXXX.tar.gz``;
4. Copy the ``XXXXXXXXX`` part of the link and use it to replace the value of the ``MODEL_NAME`` variable in the code shown below;
5. Copy the ``YYYYYYYY`` part of the link and use it to replace the value of the ``MODEL_DATE`` variable in the code shown below.

For example, the download link for the model used below is: ``download.tensorflow.org/models/object_detection/tf2/20200711/ssd_resnet101_v1_fpn_640x640_coco17_tpu-8.tar.gz``
"""
MODEL_DATE = '20200711'
MODEL_NAME = 'faster_rcnn_resnet50_v1_640x640_coco17_tpu-8'



ROOT_DIR = Path.cwd()
DATA_DIR = ROOT_DIR / 'data'
MODELS_DIR = DATA_DIR / 'models'

MODEL_TAR_FILENAME = f'{MODEL_NAME}.tar.gz'
MODELS_DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/tf2/'
MODEL_DOWNLOAD_LINK = f'{MODELS_DOWNLOAD_BASE}{MODEL_DATE}/{MODEL_TAR_FILENAME}'

PATH_TO_MODEL_TAR = MODELS_DIR / MODEL_TAR_FILENAME
PATH_TO_CKPT = MODELS_DIR / MODEL_NAME / 'checkpoint'
PATH_TO_CFG = MODELS_DIR / MODEL_NAME / 'pipeline.config'

LABEL_FILENAME = 'mscoco_label_map.pbtxt'
LABELS_DOWNLOAD_BASE = \
        'https://raw.githubusercontent.com/tensorflow/models/master/research/object_detection/data/'
PATH_TO_LABELS = MODELS_DIR / MODEL_NAME / LABEL_FILENAME


def _create_directories(model_dir: Path, data_dir: Path):
    """
    Creates the directories for the model and the data if they don't exisit already.

    Args:
        model_dir: The directory for the model.
        data_dir:  The directory for the data.

    Returns:
        None

    """
    for dir in [model_dir, data_dir]:
        if not dir.exists():
            dir.mkdir()


def _donwload_model():
    """
    Download the model from the Tensorflow 2 Model Zoo.

    Returns:
        None

    """
    if not PATH_TO_CKPT.exists():
        print('Downloading model. This may take a while... ', end='')
        urllib.request.urlretrieve(MODEL_DOWNLOAD_LINK, PATH_TO_MODEL_TAR)
        tar_file = tarfile.open(PATH_TO_MODEL_TAR)
        tar_file.extractall(MODELS_DIR)
        tar_file.close()
        os.remove(PATH_TO_MODEL_TAR)
        print('Done')

    # Download labels file    
    if not PATH_TO_LABELS.exists():
        print('Downloading label file... ', end='')
        urllib.request.urlretrieve(LABELS_DOWNLOAD_BASE + LABEL_FILENAME, PATH_TO_LABELS)
        print('Done')


def _load_model():
    """
    Returns the downloaded model.

    Returns:
        detection_model

    """
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Suppress TensorFlow logging
    tf.get_logger().setLevel('ERROR')           # Suppress TensorFlow logging (2)

    # Enable GPU dynamic memory allocation
    gpus = tf.config.experimental.list_physical_devices('GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)

    # Load pipeline config and build a detection model
    configs = config_util.get_configs_from_pipeline_file(PATH_TO_CFG)
    model_config = configs['model']
    detection_model = model_builder.build(model_config=model_config, is_training=False)
    
    # Restore checkpoint
    ckpt = tf.compat.v2.train.Checkpoint(model=detection_model)
    ckpt.restore(os.path.join(PATH_TO_CKPT, 'ckpt-0')).expect_partial()

    return detection_model


def _load_label_map():
    """
    Load label map data (for plotting)
    Label maps correspond index numbers to category names, so that when our convolution network
    predicts `5`, we know that this corresponds to `airplane`.  Here we use internal utility
    functions, but anything that returns a dictionary mapping integers to appropriate string labels
    would be fine.

    Returns:
        category_index

    """
    category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)
    return category_index


def _get_video_stream():
    """
    Return the video stream using open cv.
    https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html#capture-video-from-camera

    Returns:
        cap
    """
    cap = cv2.VideoCapture(0)
    return cap


@tf.function
def detect_fn(image, detection_model):
    """
    Object detection function.

    Args:
        image:              The image used for object detection.
        detection_model:    The object detection model used.

    Returns:
        detections, prediction_dict, tf.reshape(shapes, [-1])
    """
    image, shapes = detection_model.preprocess(image)
    prediction_dict = detection_model.predict(image, shapes)
    detections = detection_model.postprocess(prediction_dict, shapes)

    return detections, prediction_dict, tf.reshape(shapes, [-1])

def run(detection_model, label_map, video_stream):
    """
    Loads an image, runs it through the detection model and visualizes the detection results, including the keypoints.

    This will take several minutes the first time you run this.

    Here are some simple things to try out if you are curious:

    * Modify some of the input images and see if detection still works. Some simple things to try out here (just uncomment the relevant portions of code) include flipping the image horizontally, or converting to grayscale (note that we still expect the input image to have 3 channels).
    * Print out `detections['detection_boxes']` and try to match the box locations to the boxes in the image.  Notice that coordinates are given in normalized form (i.e., in the interval [0, 1]).
    * Set ``min_score_thresh`` to other values (between 0 and 1) to allow more detections in or to filter out more detections.

    Args:
        detection_model:    The object detection model used on the image.
        label_map:          The label map that consists of the classes of object.
        video_stream:       The video stream source to perform real time object detection on.

    Returns:

    """
    category_index = label_map
    cap = video_stream
    while True:
        # Read frame from camera
        ret, image_np = cap.read()

        # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image_np, axis=0)

        # TODO try: Flip horizontally
        # image_np = np.fliplr(image_np).copy()

        # TODO try: Convert image to grayscale
        # image_np = np.tile(
        #     np.mean(image_np, 2, keepdims=True), (1, 1, 3)).astype(np.uint8)

        input_tensor = tf.convert_to_tensor(np.expand_dims(image_np, 0), dtype=tf.float32)
        detections, predictions_dict, shapes = detect_fn(input_tensor, detection_model)

        label_id_offset = 1
        image_np_with_detections = image_np.copy()

        viz_utils.visualize_boxes_and_labels_on_image_array(
            image_np_with_detections,
            detections['detection_boxes'][0].numpy(),
            (detections['detection_classes'][0].numpy() + label_id_offset).astype(int),
            detections['detection_scores'][0].numpy(),
            category_index,
            use_normalized_coordinates=True,
            max_boxes_to_draw=200,
            min_score_thresh=.30,
            agnostic_mode=False)

        # Display output
        cv2.imshow('object detection', cv2.resize(image_np_with_detections, (800, 600)))

        if cv2.waitKey(25) & 0xFF == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    _create_directories(MODELS_DIR, DATA_DIR)
    _donwload_model()
    detection_model = _load_model()
    category_index = _load_label_map()   
    cap = _get_video_stream()
    run(detection_model, category_index, cap)
